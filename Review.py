import pygsheets
import datetime
from GoogleSheetConnect import LpSheet
from SheetCols import *


review_ws = LpSheet.worksheet_by_title("Ревью")

def StrToDate(string):
    return datetime.datetime(*[int(i) for i in string.split('.')[::-1]])


def GetAllRecords():
    full_records = review_ws.get_all_values()
    full_records.pop(0)
    records = []
    for row in full_records:
        if row[R_NAME] != '':
            records.append(row)
    return records

def InDateRecords(days):
    recs = GetAllRecords()
    returnRecs = []
    for row in recs:
        reviewDates = [row[R_REVIEW_DATE1],row[R_REVIEW_DATE2],row[R_REVIEW_DATE3]]
        correctDates = [i for i in reviewDates if (StrToDate(i)-datetime.datetime.today()).days+1 == days]
        if len(correctDates) > 0:
            returnRecs.append({'datarow':row,'date':correctDates[0]})
    return returnRecs
def ByNameRecords(name_list):
    recs = GetAllRecords()
    returnRecs = []
    for row in recs:
        reviewDates = [row[R_REVIEW_DATE1],row[R_REVIEW_DATE2],row[R_REVIEW_DATE3]]
        correctDates = []
        correctDates = ['none']
        if row[R_NAME] in name_list:
            returnRecs.append({'datarow':row,'date':correctDates[0]})
    return returnRecs


