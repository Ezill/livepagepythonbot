from HurmaBirthdays import GetAllEmployers, JsonDateToDate
import requests
import json
import datetime
import dateutil.relativedelta
from bs4 import BeautifulSoup


def GetWorkdayData():
    emp_list = GetAllEmployers()
    result = []
    for emp in emp_list:
        end_test_date = JsonDateToDate(emp["end_test"])
        start_work_date = end_test_date - dateutil.relativedelta.relativedelta(months=2)
        work_days = datetime.datetime.today()-start_work_date
        result.append({"id":emp["id"], "name":emp["name"],"position":emp["position"],"days":work_days.days})
    return result