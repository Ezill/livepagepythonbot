import pygsheets
from GoogleSheetConnect import LpSheet, RolesSheet
from SheetCols import *

sheet = LpSheet.worksheet_by_title("Ревью. Правила")

def GetAllRoleReviewLogic():
    full_records = sheet.get_all_values()
    full_records.pop(1)
    records = []
    for row in full_records:
        if row[RRule_Process] != 'Нет процесса' and row[RRule_Rolename] != '':
            records.append(row)
    roleList = []
    for row in records:
        array = {}
        array['name'] = row[RRule_Rolename]
        array['14'] = row[RRule_14].split(', ')
        array['7'] = row[RRule_7].split(', ')
        array['3'] = row[RRule_3].split(', ')
        roleList.append(array)
    return roleList

roleSheet = RolesSheet.worksheet_by_title("Лист1")
def Hurma_GetRoleHeirarchy():
    full_records = roleSheet.get_all_values()
    full_records.pop(0)
    records = {}
    for row in full_records:
        row.pop(0)
        if row[0] != '' and row[1] != '':
            records[row[0]]=row[1]
    return records
    