import asyncio
import aiocron
import requests
import json
import Roles
import Review
import datetime
import time
from SheetCols import *
import HurmaBirthdays
import HurmaAbsence
import HurmaNewbies
import HurmaUtils
from Review import StrToDate
import aioschedule as schedule
from dotenv import load_dotenv
from discord.ext import commands,tasks
from discord.utils import get
import discord
import pickledb
#TOKEN = "NzgxMTA2Nzc4MDY1ODYyNjY2.X740YA.oWRvByUa8Xd9KbTK8nWa6i2vuBE" #TestToken
TOKEN = "NDI1MzIyNzA3Mjg3MDE1NDM0.Wq_eow.nxe-DhFHcwJuf_gO18zKzJHC7kM"  #ProdToken

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix = "$", intents = intents)
load_dotenv()

FirstReviewNeedToSend = False


reactionInfo = []
chainInfo = {}

def GetChannelById(id):
    for guild in bot.guilds:
        for channel in guild.channels:
            if channel.id == id:
                return channel


def GetChannelByName(name):
    for guild in bot.guilds:
        for channel in guild.channels:
            if channel.name == name:
                return channel

def GetMembersByRolenames(rolenameList):
    memberList= []
    for guild in bot.guilds:
        for member in guild.members:
            for role in member.roles:
                if role.name in rolenameList and not member in memberList:
                    memberList.append(member)

    return memberList
def GetAllMembers():
    for guild in bot.guilds:
        for member in guild.members:
            print(member)
def FindRoleByName(rolename):
    for guild in bot.guilds:
        for role in guild.roles:
            if role.name == rolename:
                return role

async def Log(msg):
    ch = GetChannelByName("bot-log")
    await ch.send(datetime.datetime.today().strftime("**[%m.%d.%Y, %H:%M:%S]** "+msg))
@aiocron.crontab('0 10 * * *')
async def FirstStage():
    print("Функция: Рассылка ревью за 14 дней")
    await Log("Рассылка ревью за 14 дней. **1 этап**")
    reviewEmployers = Review.InDateRecords(14)
    for reviewData in reviewEmployers:
        in_company = (datetime.datetime.today()-StrToDate(reviewData['datarow'][R_START_DATE]))
        review_format = 'Полгода' if round(in_company.days/365,1)%1 == 0.5 else 'Год'
        embed = discord.Embed(title="Приближается ревью")
        embed.add_field(name="Сотрудник", value=reviewData['datarow'][R_NAME], inline=False)
        embed.add_field(name="Дата ревью", value=reviewData['date'], inline=False)
        embed.add_field(name="Формат ревью", value=review_format, inline=False)
        embed.add_field(name="В компании", value=f'{round(in_company.days/365,1)} лет', inline=False)
        print(reviewData['datarow'][R_NAME])
        print(reviewData['date'])
        roleRuleList = Roles.GetAllRoleReviewLogic()
        role = reviewData['datarow'][R_ROLE]
        rolesToSend = []
        for roleRule in roleRuleList:
            if roleRule['name'] == role:
                rolesToSend = roleRule['14']
        members = GetMembersByRolenames(rolesToSend)
        if members:
            await Log(f"Процесс по {reviewData['datarow'][R_NAME]}. **Дата ревью**-{reviewData['date']}")
            await Log(f"Рассылка ролям - {rolesToSend}. Пользователям - {[i.display_name for i in members]}")
            for member in members:
                print(member)
                await member.send(embed=embed)
        else:
            print(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")
            await Log(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")

            
@aiocron.crontab('15 10 * * *')
async def SecondStage():
    print("Функция: Рассылка ревью за 7 дней")
    await Log("Рассылка ревью за 7 дней. **2 этап**")
    reviewEmployers = Review.InDateRecords(7)
    for reviewData in reviewEmployers:
        title = f"У твоего коллеги {reviewData['datarow'][R_NAME]} приближается дата ревью, пожалуйста дай обратную связь о вашем взаимодействии :handshake:"
        embed = discord.Embed(title=title)
        embed.add_field(name = f'Форма опроса по ссылке :point_down:',value=f'\n{reviewData["datarow"][R_PULL_URL]}')
        roleRuleList = Roles.GetAllRoleReviewLogic()
        role = reviewData['datarow'][R_ROLE]
        print(reviewData['datarow'][R_NAME])
        print(reviewData['date'])
        rolesToSend = []
        for roleRule in roleRuleList:
            if roleRule['name'] == role:
                rolesToSend = roleRule['7']
        members = GetMembersByRolenames(rolesToSend)
        if members:
            await Log(f"Процесс по {reviewData['datarow'][R_NAME]}. **Дата ревью**-{reviewData['date']}")
            await Log(f"Рассылка ролям - {rolesToSend}. Пользователям - {[i.display_name for i in members]}")
            for member in members:
                print(member)
                await member.send(embed=embed)
        else:
            print(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")
            await Log(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")
@aiocron.crontab('0 12 * * *')
async def ThirdStage():
    print("Функция: Рассылка ревью за 3 дня")
    reviewEmployers = Review.InDateRecords(3)
    await Log("Рассылка ревью за 3 дней. **3 этап**")
    for reviewData in reviewEmployers:
        title = f"Мы собрали фидбек от команды {reviewData['datarow'][R_NAME]} по ссылке, обязательно просмотри его и подготовься к встрече :sunglasses:"
        embed = discord.Embed(title=title)
        embed.add_field(name = f':point_down:',value=f'\n{reviewData["datarow"][R_FORM_URL]}')
        roleRuleList = Roles.GetAllRoleReviewLogic()
        role = reviewData['datarow'][R_ROLE]
        print(reviewData['datarow'][R_NAME])
        print(reviewData['date'])
        rolesToSend = []
        for roleRule in roleRuleList:
            if roleRule['name'] == role:
                rolesToSend = roleRule['3']
        members = GetMembersByRolenames(rolesToSend)
        print(members)
        if members:
            await Log(f"Процесс по {reviewData['datarow'][R_NAME]}. **Дата ревью**-{reviewData['date']}")
            await Log(f"Рассылка ролям - {rolesToSend}. Пользователям - {[i.display_name for i in members]}")
            for member in members:
                print(member)
                await member.send(embed=embed)
        else:
            print(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")
            await Log(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")

embed_to_send = ''
reviewData_to_send = ''
async def SendFormToMember(member):
    global reviewData_to_send
    global embed_to_send
    embed = embed_to_send
    reviewData = reviewData_to_send
    msg = await member.send(embed=embed)
    await msg.add_reaction('🧠')
    await msg.add_reaction('💰')
    await msg.add_reaction('✅')
    reactionInfo.append(msg.id)
    print(member)
    def check(reaction,user):
        return msg.id == reaction.message.id and user.id == member.id and (reaction.emoji == '🧠' or reaction.emoji == '💰' or reaction.emoji == '✅')
    reaction, user = await bot.wait_for('reaction_add', check=check)

    msg = await member.send('**Введите комментарий**. (Или напишите **Ок**, чтобы отправить без комментария)')
    def sec_check(message):
        return message.channel == msg.channel and message.author != msg.author
    message = await bot.wait_for('message', check=sec_check)
    if message.content.lower() == 'ок':
        ch = GetChannelByName('performance_review')
        desc = f"**Дата ревью:** {reviewData['date']}\n{message.author.mention}: {reaction.emoji}"
        embed = discord.Embed(title=reviewData['datarow'][R_NAME],description=desc)
        await Log(f"{message.author} ответил в опрос-ревью о {reviewData['datarow'][R_NAME]}")
        await ch.send(embed=embed)
    else:
        ch = GetChannelByName('performance_review')
        desc = f"**Дата ревью:** {reviewData['date']}\n{message.author.mention}: {reaction.emoji} {message.content}"
        embed = discord.Embed(title=reviewData['datarow'][R_NAME],description=desc)
        await Log(f"{message.author} ответил в опрос-ревью о {reviewData['datarow'][R_NAME]}")
        await ch.send(embed=embed)
    await member.send('Ваш ответ принят!')


@aiocron.crontab('0 14 * * *')
async def FourStage():
    reviewEmployers = Review.InDateRecords(3)
    await Log("Функция: Рассылка отзыв-форм **4 этап**")
    for reviewData in reviewEmployers:
        title = f"Давай определимся с итогами ревью {reviewData['datarow'][R_NAME]} Как ты оцениваешь рост и развитие сотрудника за последние полгода?"
        embed = discord.Embed(title=title)
        embed.add_field(name = f'Высоко, нужен бонус',value =':brain:',inline=True)
        embed.add_field(name = f'Повышаем з/пл',value=':moneybag:',inline=True)
        embed.add_field(name = f'Оставляем, как есть',value=':white_check_mark:',inline=True)
        roleRuleList = Roles.GetAllRoleReviewLogic()
        role = reviewData['datarow'][R_ROLE]
        rolesToSend = []
        for roleRule in roleRuleList:
            if roleRule['name'] == role:
                rolesToSend = roleRule['3']
        members = GetMembersByRolenames(rolesToSend)
        global embed_to_send
        global reviewData_to_send
        embed_to_send = embed
        reviewData_to_send = reviewData
        if members:
            await Log(f"Процесс по {reviewData['datarow'][R_NAME]}. **Дата ревью**-{reviewData['date']}")
            await Log(f"Рассылка ролям - {rolesToSend}. Пользователям - {[i.display_name for i in members]}")
            for future in asyncio.as_completed(map(SendFormToMember, members)):
                result = await future
                    
        else:
            print(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")   

async def SendBirthDaysAtMonth(month,channel,ping):
    print("Рассылка дней рождения")
    curMonth = month
    ch = channel
    employersBdays = HurmaBirthdays.GetBirthDaysOnMonth(curMonth)
    if len(employersBdays) > 0:
        roleMention = ""
        if ping:
            roleMention = FindRoleByName('HR').mention
        await ch.send(f"{roleMention} Дни рождения сотрудников в этом месяце :")
        embed = discord.Embed(title=f"", color=0xd770b6)
        for bday in employersBdays:
            embed.add_field(name = '🎂 '+bday["name"],value =HurmaBirthdays.JsonDateToDate(bday["birth_date"]).strftime('%d.%m.%Y'),inline=False)
        await ch.send(embed=embed)

async def SendKidsBirthDaysAtMonth(month,channel,ping):
    print("Рассылка дней рождения детей")
    ch = channel
    curMonth = month
    kidsBdays = HurmaBirthdays.GetKidBDOnMonth(curMonth)
    if len(kidsBdays) > 0:
        roleMention = ""
        if ping:
            roleMention = FindRoleByName('HR').mention
        await ch.send(f"{roleMention} Дни рождения детей в этом месяце:")
        embed = discord.Embed(title=f"", color=0xd770b6)
        for bday in kidsBdays:
            embed.add_field(name = '🎂 '+bday['name'],value =f'**Родитель**:{bday["parent"]}\n**Дата рождения**: {bday["birthday"].strftime("%d.%m.%Y")}',inline=False)
        await ch.send(embed=embed)
@aiocron.crontab('4 9 1 * *')
async def BirthDayInMouth():
    print("Рассылка дней рождения")
    await Log("Рассылка дней рождений в этом месяце")
    ch = GetChannelByName('hr_birthday')
    curMonth = datetime.datetime.today().month
    await SendBirthDaysAtMonth(curMonth,ch,True)
@aiocron.crontab('5 9 1 * *')
async def KidBirthDayInMouth():
    print("Рассылка дней рождения детей")
    await Log("Функция: Рассылка дней рождений детей в этом месяце")
    ch = GetChannelByName('hr_birthday')
    curMonth = datetime.datetime.today().month
    await SendKidsBirthDaysAtMonth(curMonth,ch,True)
@aiocron.crontab('3 9 * * *')
async def DailyBDCheck():
    print("Проверка дней рождений")
    cur_day = datetime.datetime.today()
    employersBdays = HurmaBirthdays.GetBirthDaysOnMonth(cur_day.month)
    await Log("Функция: Ежедневная проверка дней рождения")
    kidsBdays = HurmaBirthdays.GetKidBDOnMonth(cur_day.month)
    ch = GetChannelByName('hr_birthday')
    roleMention = FindRoleByName('HR').mention
    for bday in employersBdays:
        birth_day = HurmaBirthdays.JsonDateToDate(bday["birth_date"])
        birth_day = datetime.datetime(cur_day.year,birth_day.month,birth_day.day)
        curPlusDay = cur_day+datetime.timedelta(days=1) 
        if birth_day.day == curPlusDay.day and birth_day.month == curPlusDay.month:
            await Log(f"Рассылка информации о дне рождения {bday['name']}")
            await ch.send(f'{roleMention} У сотрудника **{bday["name"]}** завтра день рождения! :birthday: Не забудьте его поздравить.')
    for bday in kidsBdays:
        birth_day = bday["birthday"]
        birth_day = birth_day = datetime.datetime(cur_day.year,birth_day.month,birth_day.day)
        curPlusDay = cur_day+datetime.timedelta(days=7)
        if birth_day.day == curPlusDay.day and birth_day.month == curPlusDay.month:
            await Log(f"Рассылка информации о дне рождения ребенка {bday['name']} сотрудника {bday['parent']}")
            await ch.send(f'{roleMention} Через неделю день рождения у ребенка (**{bday["name"]}**) сотрудника **{bday["parent"]}!** Ему исполняется **{bday["age"]+1}** лет :birthday:')

@aiocron.crontab('2 9 * * *')
async def VacationSend():
    print("Рассылка инфы об отпусках")
    await Log("Функция: Проверка отпусков ")
    ch = GetChannelByName('vacations')
    cur_day = datetime.datetime.today()
    vacations = HurmaAbsence.GetApprovedAbsence()
    for vacation in vacations:
        
        diff = vacation["start_date"]-cur_day
        if diff.days+1 == 1:
            if not vacation["start_time"]:
                await Log(f'С завтрашнего дня {vacation["start_date"].strftime("%d.%m.%Y")} {vacation["name"]} уходит в отпуск . К работе вернется {vacation["end_date"].strftime("%d.%m.%Y")}. Если есть задачи, которые необходимо успеть выполнить или передать в этот период, обсудите их сегодня.')
                embed = discord.Embed(title = f"{vacation['name']}", description = f'Завтра уходит в **отпуск** 🏝️. К работе вернется **{vacation["end_date"].strftime("%d.%m.%Y")}**. \nЕсли есть задачи, которые необходимо успеть выполнить или передать в этот период, обсудите их сегодня.')
                await ch.send(embed=embed)
            else:
                await Log(f'Завтра, {vacation["name"]}, с {vacation["start_time"]} до {vacation["end_time"]} будет отсутствовать.\nЕсли есть срочные задачи - обсудите сегодня. :slight_smile:')
                embed = discord.Embed(title = f"{vacation['name']}", description = f'Завтра c **{vacation["start_time"]}** до **{vacation["end_time"]}** будет **отсутствовать**.\nЕсли есть срочные задачи - обсудите сегодня. :slight_smile:')
                await ch.send(embed=embed)
        


db_sicks = pickledb.load('sended_sicks.db', False)
@aiocron.crontab('30 * * * *')
async def SickRequestCheck():
    print("Проверка новых заявок на больничный")
    ch = GetChannelByName('vacations')
    await Log("Функция: Проверка больничных ")
    vacations = HurmaAbsence.GetNewAbsence()
    for vacation in vacations:
        if not db_sicks.get(vacation["id"]):
            db_sicks.set(vacation["id"],"Отправлено")
            await Log(f'{vacation["name"]} c {vacation["start_date"].strftime("%d.%m.%Y")} по {vacation["end_date"].strftime("%d.%m.%Y")} на больничном :thermometer_face:. За перераспределение задач отвечает менеджер направления.')
            embed = discord.Embed(title = f"{vacation['name']} :thermometer:", description = f'C **{vacation["start_date"].strftime("%d.%m.%Y")}** по **{vacation["end_date"].strftime("%d.%m.%Y")}** на **больничном** 🤒 \nЗа перераспределение задач отвечает менеджер направления. ')
            await ch.send(embed=embed)            
            db_sicks.dump()
        else:
            print("Найдена заявка на больничный, но уже была отправлена",vacation["name"],vacation["start_date"].strftime("%d.%m.%Y"))

@aiocron.crontab('1 12 * * *')
async def NewbieFirstStages():
    print("Рассылка нотификаций о новичках")
    await Log("Функция: Нотифакции по новым сотрудникам")
    data = HurmaNewbies.GetWorkdayData()
    heirarchy = Roles.Hurma_GetRoleHeirarchy()
    today = datetime.datetime(2021,1,28)
    for emp in data:
        days = emp["days"]
        name = emp["name"]

        if (days == 1 and (today.weekday() not in [5,6])) or (days in[2,3] and today.weekday()==0):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR"])]}')
            await Log(f'Привет, сегодня +1 сотрудник в команде Livepage - {name}. Не забудь провести с ним Welcome Meeting в начале рабочего дня.')
            for member in GetMembersByRolenames(["HR"]):
                await member.send(f'Привет, сегодня +1 сотрудник в команде Livepage - {name}. Не забудь провести с ним Welcome Meeting в начале рабочего дня.')
        elif (days == 5 and (today.weekday() not in [5,6])) or (days in[6,7] and today.weekday()==0):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR"])]}')
            await Log(f'Привет, сегодня 5 день работы {name}, проведи с ним 1:1, уточни как прошла 1 неделя обучения, достаточно ли сотруднику уделили времени и внимания.')
            for member in GetMembersByRolenames(["HR"]):
                await member.send(f'Привет, сегодня 5 день работы {name}, проведи с ним 1:1, уточни как прошла 1 неделя обучения, достаточно ли сотруднику уделили времени и внимания.')
        elif (days == 14 and (today.weekday() not in [5,6])) or (days in[15,16] and today.weekday()==0):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR",heirarchy[emp["position"]]])]}')
            await Log(f'Привет, сегодня 14 день работы {name}, проведи с ним 1:1, уточни какой прогресс его обучения.')
            for member in GetMembersByRolenames(["HR",heirarchy[emp['position']]]):
                await member.send(f'Привет, сегодня 14 день работы {name}, проведи с ним 1:1, уточни какой прогресс его обучения.')
        elif (days == 30 and (today.weekday() not in [5,6])) or (days in[31,32] and today.weekday()==0):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR",heirarchy[emp["position"]]])]}')
            await Log(f'Привет, прошел уже месяц стажировки у {name}. Обязательно проведи с ним Adaptation Meeting, уточни его прогресс и как проходит его адаптация. Собери обратную связь от ментора и команды взаимодействия.')
            for member in GetMembersByRolenames(["HR",heirarchy[emp['position']]]):
                await member.send(f'Привет, прошел уже месяц стажировки у {name}. Обязательно проведи с ним Adaptation Meeting, уточни его прогресс и как проходит его адаптация. Собери обратную связь от ментора и команды взаимодействия.')
        elif (days == 58):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR",heirarchy[emp["position"]]])]}')
            await Log(f'Привет! Через 2 дня заканчивается стажировка у {name}. Пора подвести ее итоги.')
            for member in GetMembersByRolenames([heirarchy[emp['position']]]):
                await member.send(f'Привет! Через 2 дня заканчивается стажировка у {name}. Пора подвести ее итоги.') 
        elif (days == 60 and (today.weekday() not in [5,6])) or (days in[61,62] and today.weekday()==0):
            await Log(f'Процесс по {name}. Рассылка пользователям - {[i.display_name for i in GetMembersByRolenames(["HR"])]}')
            await Log(f'Привет, у {name} сегодня заканчивается испытательный срок. Проведи с ним встречу, чтобы подвести итоги стажировки.') 
            for member in GetMembersByRolenames(["HR"]):
                await member.send(f'Привет, у {name} сегодня заканчивается испытательный срок. Проведи с ним встречу, чтобы подвести итоги стажировки.') 


newbie_embed_to_send = ''
newbie_name = ''
async def SendNewbieForm(member):
    global newbie_embed_to_send
    global newbie_name
    embed = newbie_embed_to_send
    name = newbie_name
    await Log(f'Процесс-форма по {name}. Отправлено пользователю {member}')
    msg = await member.send(embed=embed)
    await msg.add_reaction('🙁')
    await msg.add_reaction('🕓')
    await msg.add_reaction('🥳')
    reactionInfo.append(msg.id)
    def check(reaction,user):
        return msg.id == reaction.message.id and user.id == member.id and (reaction.emoji == '🙁' or reaction.emoji == '🕓' or reaction.emoji == '🥳')
    reaction, user = await bot.wait_for('reaction_add', check=check)

    msg = await member.send('**Введите комментарий или сумму в случае пройденной стажировки**. (Или напишите **Ок**, чтобы отправить без комментария)')
    def sec_check(message):
        return message.channel == msg.channel and message.author != msg.author
    message = await bot.wait_for('message', check=sec_check)
    if message.content.lower() == 'ок':
        ch = GetChannelByName('performance_review')
        msg=''
        if reaction.emoji == '🙁':
            msg = f"{name}\nНе прошел стажировку 🙁\n<@252809501604839435> необходимо произвести расчет"
        elif reaction.emoji == '🕓':
            msg = f"{name}\nПродолжает быть на стажировке еще месяц 🕓\n<@252809501604839435>, остается на том же уровне еще месяц"
        elif reaction.emoji == '🥳':
            msg = f"{name}\nУспешно прошел стажировку 🥳 \n<@252809501604839435>, сумма ЗП не указана главой отдела {message.author.mention}"
        await ch.send(msg)
    else:
        ch = GetChannelByName('performance_review')
        msg=''
        if reaction.emoji == '🙁':
            msg = f"{name}\nНе прошел стажировку 🙁\n<@252809501604839435> необходимо произвести расчет\nКомментарий от {message.author.mention}: {message.content}"
        elif reaction.emoji == '🕓':
            msg = f"{name}\nПродолжает быть на стажировке еще месяц 🕓\n<@252809501604839435>, остается на том же уровне еще месяц\nКомментарий от {message.author.mention}: {message.content}"
        elif reaction.emoji == '🥳':
            msg = f"{name}\nУспешно прошел стажировку 🥳 \n<@252809501604839435>, сумма после стажировки - {message.author.mention}: {message.content}"
        await ch.send(msg)
    await member.send('Ваш ответ принят!')


@aiocron.crontab('0 15 * * *')
async def NewbieVoteStage():
    print("Рассылка нотификаций о новичках")
    data = HurmaNewbies.GetWorkdayData()
    heirarchy = Roles.Hurma_GetRoleHeirarchy()
    await Log("Функция: Рассылка формы последнего дня испытательного срока")
    today = datetime.datetime(2021,1,28)
    for emp in data:
        days = emp["days"]
        name = emp["name"]
        if (days == 58):
            title = f"Давай определимся с суммой з/пл для {name} по итогу 2х месяцев."
            embed = discord.Embed(title=title)
            embed.add_field(name = f'Не проходит стажировку (увольнение)',value ='🙁',inline=True)
            embed.add_field(name = f'Продлеваем стажировку на месяц (з/пл не меняется) ',value='🕓',inline=True)
            embed.add_field(name = f'Проходит стажировку (сумма после стажировки)',value='🥳',inline=True)
            rolesToSend = heirarchy[emp['position']]
            members = GetMembersByRolenames(rolesToSend)
            global newbie_embed_to_send
            global newbie_name
            newbie_embed_to_send = embed
            newbie_name = name
            if members:
                for future in asyncio.as_completed(map(SendNewbieForm, members)):
                    result = await future
        
   
@aiocron.crontab('45 10 * * *')
async def SendAnniversaries():
    print("Рассылка нотификаций о годовщинах")
    await Log("Функция: Рассылка нотификация по годовщинам")
    data = HurmaNewbies.GetWorkdayData()
    ch = GetChannelByName('📣announcements')
    for emp in data:
        days = emp["days"]
        name = emp["name"]

        if (days %365 == 0) and days//365 > 0:
            embed = discord.Embed(title=f"", color=0xd770b6)
            emp_data = HurmaUtils.GetDataFromTextId(emp['id'])
            embed.set_thumbnail(url=emp_data['avatarSrc'])
            await Log(f'Сегодня отмечает свою **{days//365}** годовщину в Livepage!')
            embed.add_field(name=name,value= f'Сегодня отмечает свою **{days//365}** годовщину в Livepage! 🎉')
            await ch.send(embed=embed)

@bot.event
async def on_ready():
    print('Бот запущен')
    
@bot.event
async def on_message(message):
    if message.content.startswith('!birthdays '):
        args = message.content.split()
        month = int(args[1])
        if month:
            ch = message.channel
            await SendBirthDaysAtMonth(month,ch,False)
    elif message.content.startswith('!kidbirthdays '):
        args = message.content.split()
        month = int(args[1])
        if month:
            ch = message.channel
            await SendKidsBirthDaysAtMonth(month,ch,False)
    elif message.content.startswith('!sendreviewform '):
        names = message.content[16:].split(';')
        reviewEmployers = Review.ByNameRecords(names)
        for reviewData in reviewEmployers:
            print(reviewData)
            title = f"Давай определимся с итогами ревью {reviewData['datarow'][R_NAME]} Как ты оцениваешь рост и развитие сотрудника за последние полгода?"
            embed = discord.Embed(title=title)
            embed.add_field(name = f'Высоко, нужен бонус',value =':brain:',inline=True)
            embed.add_field(name = f'Повышаем з/пл',value=':moneybag:',inline=True)
            embed.add_field(name = f'Оставляем, как есть',value=':white_check_mark:',inline=True)
            roleRuleList = Roles.GetAllRoleReviewLogic()
            role = reviewData['datarow'][R_ROLE]
            rolesToSend = []
            for roleRule in roleRuleList:
                if roleRule['name'] == role:
                    rolesToSend = roleRule['3']
            members = GetMembersByRolenames(rolesToSend)
            global embed_to_send
            global reviewData_to_send
            embed_to_send = embed
            reviewData_to_send = reviewData
            if members:
                for future in asyncio.as_completed(map(SendFormToMember, members)):
                    result = await future
                        
            else:
                print(f"Ролей на рассылку информации о {reviewData['datarow'][R_NAME]} не найдено")
bot.run(TOKEN)
