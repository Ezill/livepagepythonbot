import HurmaConnect
import requests
import json
import datetime
from bs4 import BeautifulSoup

MONTH_STRING = {'янв':1,
                'фев':2,
                'март':3,
                'апр':4,
                'мая':5,
                'июня':6,
                'июля':7,
                'авг':8,
                'сен':9,
                'окт':10,
                'ноя':11,
                'дек':12}

token_headers={'token':'8b0167210ae90953d0a51cec89ee6e7bb17a65c567e97b7d9f41e8b321f59e7c907dc1e1294502e'}

def JsonDateToDate(string):
    values = string.split('-')
    return datetime.datetime(int(values[0]),int(values[1]),int(values[2]))

def GetAllEmployers():
    response = requests.get('https://livepage.hurma.work/api/v1/employees?per_page=200',headers=token_headers)
    employersData =  json.loads(response.text)
    return employersData['result']['data']
def GetBirthDaysOnMonth(month):
    employersList = GetAllEmployers()
    resultList = []
    for emp in employersList:
        if emp["birth_date"]:
            bd = JsonDateToDate(emp["birth_date"])
            if bd and bd.month == month:
                resultList.append(emp)
    return resultList

def GetAllKidsData():
    kidsUrl = f'https://livepage.hurma.work/all-children-table'
    kidsData = HurmaConnect.session.get(kidsUrl,headers=HurmaConnect.headers)
    rawkidsList =  json.loads(kidsData.text)['data']
    kidsList = []
    for rawKid in rawkidsList:
        age = rawKid['raw_age']
        raw_bd = BeautifulSoup(rawKid['birth_date'],features="html.parser").get_text().split()
        bd = datetime.datetime(int(raw_bd[2]),int(MONTH_STRING[raw_bd[1]]),int(raw_bd[0]))
        name = BeautifulSoup(rawKid['name'],features="html.parser").get_text()
        parent_name = BeautifulSoup(rawKid['parent_name'],features="html.parser").get_text()[:-3]

        child = {'name':name,'age':age,'birthday':bd,'parent':parent_name}
        kidsList.append(child)
    return kidsList
def GetKidBDOnMonth(month):
    kidList = GetAllKidsData()
    resultList = []
    for kid in kidList:
        bd = kid["birthday"]
        if bd:
            if bd and bd.month == month:
                resultList.append(kid)
    return resultList
