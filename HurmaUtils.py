import HurmaConnect
import datetime
import json

from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest'
}

def GetDataFromTextId(id):
    url = f'https://livepage.hurma.work/employees/{id}'
    soup = BeautifulSoup(HurmaConnect.session.get(url,headers=headers).text,'html.parser')
    html = HurmaConnect.session.get(url,headers=headers).text
    numeric_id = soup.find("div",{"id":'employee-tasks'})["data-people-id"]
    url = f'https://livepage.hurma.work/employee/vue/common/info?employee_id={numeric_id}'
    data = HurmaConnect.session.get(url,headers=headers)
    json_data = json.loads(data.text)
    return json_data["data"]
