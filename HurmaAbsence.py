import HurmaConnect
import datetime
import json

from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest'
}

from bs4 import BeautifulSoup

def GetApprovedAbsence():
    url = f'https://livepage.hurma.work/absence-request/getArchiveRequest?draw=3&columns[0][data]=avatar&columns[0][name]=&columns[0][searchable]=false&columns[0][orderable]=false&columns[0][search][value]=&columns[0][search][regex]=false&columns[1][data]=employee&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=true&columns[1][search][value]=&columns[1][search][regex]=false&columns[2][data]=dates&columns[2][name]=&columns[2][searchable]=false&columns[2][orderable]=true&columns[2][search][value]=&columns[2][search][regex]=false&columns[3][data]=date_of_request&columns[3][name]=&columns[3][searchable]=false&columns[3][orderable]=true&columns[3][search][value]=&columns[3][search][regex]=false&columns[4][data]=status&columns[4][name]=&columns[4][searchable]=false&columns[4][orderable]=true&columns[4][search][value]=&columns[4][search][regex]=false&columns[5][data]=actions_request&columns[5][name]=&columns[5][searchable]=false&columns[5][orderable]=true&columns[5][search][value]=&columns[5][search][regex]=false&order[0][column]=3&order[0][dir]=desc&start=0&length=1000000&search[value]=&search[regex]=false&_=1611142130050'
    data = HurmaConnect.session.get(url,headers=headers)
    
    dic_data =  json.loads(data.text)
    result = []
    for i in dic_data['data']:
        name = i["name"]
        abs_type = BeautifulSoup(i["type_of_request"],features="html.parser").get_text()
        json_dates = json.loads((i["data"]).replace('&quot','"').replace('";','"'))
        start_date = datetime.datetime(*[int(i) for i in json_dates["start_date"].split(".")[::-1]])
        end_date = datetime.datetime(*[int(i) for i in json_dates["end_date"].split(".")[::-1]]) + datetime.timedelta(days=1)
        if 'ok-icon' in i["status"] and not 'Больничный' in i["type_of_request"]:
            result.append({"name":name,
                            "abs_type":abs_type,
                            "start_date":start_date,
                            "end_date":end_date,
                            "start_time":json_dates["start_time"],
                            "end_time":json_dates["end_time"]})
                        
    return result

def GetNewAbsence():
    url = f'https://livepage.hurma.work/absence-request/getActiveRequest?draw=3&columns%5B0%5D%5Bdata%5D=avatar&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=employee&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=dates&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B3%5D%5Bdata%5D=minus_days&columns%5B3%5D%5Bname%5D=&columns%5B3%5D%5Bsearchable%5D=true&columns%5B3%5D%5Borderable%5D=true&columns%5B3%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B3%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B4%5D%5Bdata%5D=state&columns%5B4%5D%5Bname%5D=&columns%5B4%5D%5Bsearchable%5D=true&columns%5B4%5D%5Borderable%5D=false&columns%5B4%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B4%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B5%5D%5Bdata%5D=date_of_request&columns%5B5%5D%5Bname%5D=&columns%5B5%5D%5Bsearchable%5D=true&columns%5B5%5D%5Borderable%5D=true&columns%5B5%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B5%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B6%5D%5Bdata%5D=actions_request&columns%5B6%5D%5Bname%5D=&columns%5B6%5D%5Bsearchable%5D=true&columns%5B6%5D%5Borderable%5D=true&columns%5B6%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B6%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B7%5D%5Bdata%5D=raw_date_of_request&columns%5B7%5D%5Bname%5D=raw_date_of_request&columns%5B7%5D%5Bsearchable%5D=true&columns%5B7%5D%5Borderable%5D=true&columns%5B7%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B7%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B8%5D%5Bdata%5D=raw_type_of_request&columns%5B8%5D%5Bname%5D=raw_type_of_request&columns%5B8%5D%5Bsearchable%5D=true&columns%5B8%5D%5Borderable%5D=true&columns%5B8%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B8%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B9%5D%5Bdata%5D=raw_dates&columns%5B9%5D%5Bname%5D=raw_dates&columns%5B9%5D%5Bsearchable%5D=true&columns%5B9%5D%5Borderable%5D=true&columns%5B9%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B9%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B10%5D%5Bdata%5D=employee_name&columns%5B10%5D%5Bname%5D=employee_name&columns%5B10%5D%5Bsearchable%5D=true&columns%5B10%5D%5Borderable%5D=true&columns%5B10%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B10%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B11%5D%5Bdata%5D=raw_minus_days&columns%5B11%5D%5Bname%5D=raw_minus_days&columns%5B11%5D%5Bsearchable%5D=true&columns%5B11%5D%5Borderable%5D=true&columns%5B11%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B11%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=7&order%5B0%5D%5Bdir%5D=desc&start=0&length=1000000&search%5Bvalue%5D=&search%5Bregex%5D=false&_=1611410068472'
    data = HurmaConnect.session.get(url,headers=headers)
    
    dic_data =  json.loads(data.text)
    result = []
    for i in dic_data['data']:
        name = BeautifulSoup(i["employee_name"],features="html.parser").get_text()
        abs_type = BeautifulSoup(i["type_of_request"],features="html.parser").get_text()
        json_dates = json.loads((i["data"]).replace('&quot','"').replace('";','"'))
        start_date = datetime.datetime(*[int(i) for i in json_dates["start_date"].split(".")[::-1]])
        end_date = datetime.datetime(*[int(i) for i in json_dates["end_date"].split(".")[::-1]]) + datetime.timedelta(days=1)
        if 'Больничный' in i["type_of_request"]:
            result.append({"name":name,
                            "abs_type":abs_type,
                            "start_date":start_date,
                            "end_date":end_date,
                            "id": str(i["event_people_id"])})
                        
    return result
