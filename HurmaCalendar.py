import HurmaConnect
import datetime
import json
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest'
}




def GetCalendarData(date_from, date_to):
    calendarUrl = f'https://livepage.hurma.work/calendar/api?startDate={date_from}&endDate={date_to}'
    calendarData = HurmaConnect.session.get(calendarUrl,headers=headers)
    print(calendarData.text)
    calendarJson =  json.loads(calendarData.text)
    return calendarJson